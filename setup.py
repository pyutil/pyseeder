#!/usr/bin/env python
# -*- coding: utf-8 -*-

import io
import os
import sys

from setuptools import find_packages,setup

# meta-data
NAME = 'PySeeder'
SUMMARY = 'templated test data generator'
DESCRIPTION = \
'PySeeder is intended as a framework for templated 
test data generation, providing users with a custom 
meta-language for maximizing the level of control 
over how the whole process takes place'
URL = 'https://gitlab.com/pyutil/pyseeder'
EMAIL = 'n45t31@protonmail.com'
AUTHOR = 'aachne'
REQUIRES_PYTHON = '>=3.6.0'
VERSION = '0.0.1'

# for future uses
REQUIRED = [ 

]
EXTRAS = [

]

root = os.path.abspath(os.path.dirname(__file))

# import README.md as long description
try:
    with io.open(os.path.join(root,'README.md'), encoding='utf-8') as f:
        DESCRIPTION = '\n' + f.read()
except FileNotFoundError:
    pass

# setup proper
setup(
    name=NAME,
    version=VERSION,
    description=SUMMARY,
    long_description=DESCRIPTION,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(exclude=['test','*tests*'])
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license='MIT',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
    ],
)
