.if !\n(.g .ab GNU tbl requires GNU troff.
.if !dTS .ds TS
.if !dTE .ds TE
.lf 1 1.man
.TH PYSEEDER 1 2019-05-29 GNU "PySeeder User's Manual"
.SH NAME
.RS 1
pyseeder \- configurable framework for test data generation
.RE
.SH SYNOPSIS
.RS 1
.B pyseeder 
\-h|\-\-version
.EE
.B pyseeder 
[\-vqs] \-\-config file
.EE
.B pyseeder 
[\-vqs] [\-o dir] [\-i files...|\-d dir] \-c file|\(\-c class num\)
.RE
.SH DESCRIPTION
.RS 1
PySeeder parses user\-defined data entity templates to create
internal class hierarchy, which it in turn uses to generate 
corresponding JSON data files following generator config
specified as [OPTIONS] or [\-c file] argument.
.RE
.SH OPTIONS
.RS 1
.SS generator config
.TS
.nr 3c \n(.C
.cp 0
.nr 3lps \n[.s]
.nr 3cent \n[.ce]
.de 3init
.ft \n[.f]
.ps \n[.s]
.vs \n[.v]u
.in \n[.i]u
.ll \n[.l]u
.ls \n[.L]
.ad \n[.j]
.ie \n[.u] .fi
.el .nf
.ce \n[.ce]
..
.nr 3ind \n[.i]
.nr 3fnt \n[.f]
.nr 3sz \n[.s]
.nr 3fll \n[.u]
.nr T. 0
.nr 3crow 0-1
.nr 3passed 0-1
.nr 3sflag 0
.ds 3trans
.ds 3quote
.nr 3brule 1
.nr 3supbot 0
.eo
.de 3rmk
.mk \$1
.if !'\n(.z'' \!.3rmk "\$1"
..
.de 3rvpt
.vpt \$1
.if !'\n(.z'' \!.3rvpt "\$1"
..
.de 3rlns
.ie !'\n(.z'' \{.nm
\!.3rlns "\$1"
.\}
.el .if \n[ln] \{\
.if '\$1'd' .nr 3lnst \n[ln]
.if '\$1's' .nm \n[3lnst]
.if '\$1'm' .nr 3lnmx \n[ln]>?\n[3lnmx]
.\}
..
.de 3rlnx
.ie !'\n(.z'' \{.nm
\!.3rlnx "\$1"
.\}
.el .if \n[ln] \{\
.ie '\$1's' \{\
.nr 3lnsv \n(ln<?\n[3lnmx]
.nm +0 \n[ln]+42
.\}
.el \{\
.nr ln \n[3lnsv]
.nm \n[ln] 1
.\}
.\}
..
.de 3keep
.if '\n[.z]'' \{.ds 3quote \\
.ds 3trans \!
.di 3section
.nr 3sflag 1
.in 0
.\}
..
.ig
.EQ
delim off
.EN
..
.de 3release
.if \n[3sflag] \{.di
.in \n[3ind]u
.nr 3dn \n[dn]
.ds 3quote
.ds 3trans
.nr 3sflag 0
.if \n[.t]<=\n[dn] \{.nr T. 1
.T#
.nr 3supbot 1
.sp \n[.t]u
.nr 3supbot 0
.mk #T
.\}
.if \n[.t]<=\n[3dn] .tm warning: page \n%: table text block will not fit on one page
.nf
.if \n[ln] .nm \n[ln]
.nr 3lnmx \n[ln]
.ls 1
.3section
.ls
.if \n[ln] .nm
.rm 3section
.\}
..
.ig
.EQ
delim on
.EN
..
.nr 3tflag 0
.de 3tkeep
.if '\n[.z]'' \{.di 3table
.nr 3tflag 1
.\}
..
.de 3trelease
.if \n[3tflag] \{.br
.di
.nr 3dn \n[dn]
.ne \n[dn]u+\n[.V]u
.ie \n[.t]<=\n[3dn] .tm error: page \n%: table will not fit on one page; use .TS H/.TH with a supporting macro package
.el \{.in 0
.ls 1
.nf
.if \n[ln] .nm \n[ln]
.3table
.\}
.rm 3table
.\}
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
..
.ec
.ce 0
.nf
.nr 3sep 1n
.nr 3w0 \n(.H
.nr 3aw0 0
.nr 3lnw0 0
.nr 3rnw0 0
.nr 3w1 \n(.H
.nr 3aw1 0
.nr 3lnw1 0
.nr 3rnw1 0
.nr 3w2 \n(.H
.nr 3aw2 0
.nr 3lnw2 0
.nr 3rnw2 0
.lf 30 1.man
.nr 3w0 \n[3w0]>?\w\[tbl]   \-\-config\[tbl]
.lf 30
.nr 3w1 \n[3w1]>?\w\[tbl]\fIfile\fR\[tbl]
.lf 30
.nr 3w2 \n[3w2]>?\w\[tbl]specifies configuration file to use for\[tbl]
.lf 31
.nr 3w2 \n[3w2]>?\w\[tbl]generation, overrides any other config\[tbl]
.lf 32
.nr 3w0 \n[3w0]>?\w\[tbl]\-o|\-\-output\[tbl]
.lf 32
.nr 3w1 \n[3w1]>?\w\[tbl]\fIdir\fR\[tbl]
.lf 32
.nr 3w2 \n[3w2]>?\w\[tbl]specifies directory under which output \[tbl]
.lf 33
.nr 3w2 \n[3w2]>?\w\[tbl]files are to be stored or stdout if null\[tbl]
.lf 34
.nr 3w0 \n[3w0]>?\w\[tbl]\-i|\-\-input\[tbl]
.lf 34
.nr 3w1 \n[3w1]>?\w\[tbl]\fIfiles\.\.\.\fR\[tbl]
.lf 34
.nr 3w2 \n[3w2]>?\w\[tbl]tells PySeeder to parse and register\[tbl]
.lf 35
.nr 3w2 \n[3w2]>?\w\[tbl]everything written in $files\[tbl]
.lf 36
.nr 3w0 \n[3w0]>?\w\[tbl]\-d|\-\-directory\[tbl]
.lf 36
.nr 3w1 \n[3w1]>?\w\[tbl]\fIdir\fR\[tbl]
.lf 36
.nr 3w2 \n[3w2]>?\w\[tbl]tells PySeeder to recursively parse\[tbl]
.lf 37
.nr 3w2 \n[3w2]>?\w\[tbl]everything contained under $dir\[tbl]
.lf 38
.nr 3w0 \n[3w0]>?\w\[tbl]\-c|\-\-create\[tbl]
.lf 38
.nr 3w1 \n[3w1]>?\w\[tbl]\fIclass\fR \fInum\fR\[tbl]
.lf 38
.nr 3w2 \n[3w2]>?\w\[tbl]signalises that $num instances of \[tbl]
.lf 39
.nr 3w2 \n[3w2]>?\w\[tbl]$class are to be created. $class\[tbl]
.lf 40
.nr 3w2 \n[3w2]>?\w\[tbl]must be previously registered\[tbl]
.lf 41
.nr 3w1 \n[3w1]>?\w\[tbl]\fIfile\fR\[tbl]
.lf 41
.nr 3w2 \n[3w2]>?\w\[tbl]specifies generation command file\[tbl]
.nr 3w0 \n[3w0]>?(\n[3lnw0]+\n[3rnw0])
.if \n[3aw0] .nr 3w0 \n[3w0]>?(\n[3aw0]+2n)
.nr 3w1 \n[3w1]>?(\n[3lnw1]+\n[3rnw1])
.if \n[3aw1] .nr 3w1 \n[3w1]>?(\n[3aw1]+2n)
.nr 3w2 \n[3w2]>?(\n[3lnw2]+\n[3rnw2])
.if \n[3aw2] .nr 3w2 \n[3w2]>?(\n[3aw2]+2n)
.nr 3expand \n[.l]-\n[.i]-\n[3w0]-\n[3w1]-\n[3w2]-6n
.if \n[3expand]<0 \{\
.lf 30
.ig
.EQ
delim off
.EN
..
.tm1 "warning: file `\n[.F]', around line \n[.c]:
.tm1 "  table wider than line width
.ig
.EQ
delim on
.EN
..
.nr 3expand 0
.\}
.nr 3cd0 0
.nr 3cl0 0*\n[3sep]
.nr 3ce0 \n[3cl0]+\n[3w0]
.nr 3cl1 \n[3ce0]+(3*\n[3sep])
.nr 3cd1 \n[3ce0]+\n[3cl1]/2
.nr 3ce1 \n[3cl1]+\n[3w1]
.nr 3cl2 \n[3ce1]+(3*\n[3sep])
.nr 3cd2 \n[3ce1]+\n[3cl2]/2
.nr 3ce2 \n[3cl2]+\n[3w2]
.nr 3cd3 \n[3ce2]+(0*\n[3sep])
.nr TW \n[3cd3]
.if \n[3cent] \{.in +(u;\n[.l]-\n[.i]-\n[TW]/2>?-\n[.i])
.nr 3ind \n[.i]
.\}
.eo
.ig
.EQ
delim off
.EN
..
.de T#
.if !\n[3supbot] \{.3rvpt 0
.mk 3vert
.3rlnx s
.ls 1
.3rlnx r
.ls
.nr 3passed \n[3crow]
.sp |\n[3vert]u
.3rvpt 1
.\}
..
.ig
.EQ
delim on
.EN
..
.ec
.fc 
.3keep
.3rmk 3rt0
\*[3trans].nr 3crow 0
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs0
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 30
\&\h'|\n[3cl0]u'   \-\-config\h'|\n[3cl1]u'\fIfile\fR\h'|\n[3cl2]u'specifies configuration file to use for
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs0]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt1
\*[3trans].nr 3crow 1
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs1
.mk 3bot
.3rvpt 0
.ta \n[3ce2]u
.lf 31
\&\h'|\n[3cl2]u'generation, overrides any other config
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs1]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt2
\*[3trans].nr 3crow 2
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs2
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 32
\&\h'|\n[3cl0]u'\-o|\-\-output\h'|\n[3cl1]u'\fIdir\fR\h'|\n[3cl2]u'specifies directory under which output 
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs2]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt3
\*[3trans].nr 3crow 3
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs3
.mk 3bot
.3rvpt 0
.ta \n[3ce2]u
.lf 33
\&\h'|\n[3cl2]u'files are to be stored or stdout if null
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs3]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt4
\*[3trans].nr 3crow 4
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs4
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 34
\&\h'|\n[3cl0]u'\-i|\-\-input\h'|\n[3cl1]u'\fIfiles\.\.\.\fR\h'|\n[3cl2]u'tells PySeeder to parse and register
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs4]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt5
\*[3trans].nr 3crow 5
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs5
.mk 3bot
.3rvpt 0
.ta \n[3ce2]u
.lf 35
\&\h'|\n[3cl2]u'everything written in $files
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs5]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt6
\*[3trans].nr 3crow 6
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs6
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 36
\&\h'|\n[3cl0]u'\-d|\-\-directory\h'|\n[3cl1]u'\fIdir\fR\h'|\n[3cl2]u'tells PySeeder to recursively parse
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs6]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt7
\*[3trans].nr 3crow 7
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs7
.mk 3bot
.3rvpt 0
.ta \n[3ce2]u
.lf 37
\&\h'|\n[3cl2]u'everything contained under $dir
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs7]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt8
\*[3trans].nr 3crow 8
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs8
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 38
\&\h'|\n[3cl0]u'\-c|\-\-create\h'|\n[3cl1]u'\fIclass\fR \fInum\fR\h'|\n[3cl2]u'signalises that $num instances of 
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs8]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt9
\*[3trans].nr 3crow 9
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs9
.mk 3bot
.3rvpt 0
.ta \n[3ce2]u
.lf 39
\&\h'|\n[3cl2]u'$class are to be created. $class
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs9]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt10
\*[3trans].nr 3crow 10
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs10
.mk 3bot
.3rvpt 0
.ta \n[3ce2]u
.lf 40
\&\h'|\n[3cl2]u'must be previously registered
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs10]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt11
\*[3trans].nr 3crow 11
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs11
.mk 3bot
.3rvpt 0
.ta \n[3ce1]u \n[3ce2]u
.lf 41
\&\h'|\n[3cl1]u'\fIfile\fR\h'|\n[3cl2]u'specifies generation command file
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs11]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.if \n[ln] .nr ln \n[3lnmx]
.3release
.mk 3rt12
.nr 3brule 1
.nr T. 1
.ig
.EQ
delim off
.EN
..
.T#
.ig
.EQ
delim on
.EN
..
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
.3init
.fc
.cp \n(3c
.lf 42
.TE
.SS output verbosity
.TS
.nr 3c \n(.C
.cp 0
.nr 3lps \n[.s]
.nr 3cent \n[.ce]
.de 3init
.ft \n[.f]
.ps \n[.s]
.vs \n[.v]u
.in \n[.i]u
.ll \n[.l]u
.ls \n[.L]
.ad \n[.j]
.ie \n[.u] .fi
.el .nf
.ce \n[.ce]
..
.nr 3ind \n[.i]
.nr 3fnt \n[.f]
.nr 3sz \n[.s]
.nr 3fll \n[.u]
.nr T. 0
.nr 3crow 0-1
.nr 3passed 0-1
.nr 3sflag 0
.ds 3trans
.ds 3quote
.nr 3brule 1
.nr 3supbot 0
.eo
.de 3rmk
.mk \$1
.if !'\n(.z'' \!.3rmk "\$1"
..
.de 3rvpt
.vpt \$1
.if !'\n(.z'' \!.3rvpt "\$1"
..
.de 3rlns
.ie !'\n(.z'' \{.nm
\!.3rlns "\$1"
.\}
.el .if \n[ln] \{\
.if '\$1'd' .nr 3lnst \n[ln]
.if '\$1's' .nm \n[3lnst]
.if '\$1'm' .nr 3lnmx \n[ln]>?\n[3lnmx]
.\}
..
.de 3rlnx
.ie !'\n(.z'' \{.nm
\!.3rlnx "\$1"
.\}
.el .if \n[ln] \{\
.ie '\$1's' \{\
.nr 3lnsv \n(ln<?\n[3lnmx]
.nm +0 \n[ln]+42
.\}
.el \{\
.nr ln \n[3lnsv]
.nm \n[ln] 1
.\}
.\}
..
.de 3keep
.if '\n[.z]'' \{.ds 3quote \\
.ds 3trans \!
.di 3section
.nr 3sflag 1
.in 0
.\}
..
.ig
.EQ
delim off
.EN
..
.de 3release
.if \n[3sflag] \{.di
.in \n[3ind]u
.nr 3dn \n[dn]
.ds 3quote
.ds 3trans
.nr 3sflag 0
.if \n[.t]<=\n[dn] \{.nr T. 1
.T#
.nr 3supbot 1
.sp \n[.t]u
.nr 3supbot 0
.mk #T
.\}
.if \n[.t]<=\n[3dn] .tm warning: page \n%: table text block will not fit on one page
.nf
.if \n[ln] .nm \n[ln]
.nr 3lnmx \n[ln]
.ls 1
.3section
.ls
.if \n[ln] .nm
.rm 3section
.\}
..
.ig
.EQ
delim on
.EN
..
.nr 3tflag 0
.de 3tkeep
.if '\n[.z]'' \{.di 3table
.nr 3tflag 1
.\}
..
.de 3trelease
.if \n[3tflag] \{.br
.di
.nr 3dn \n[dn]
.ne \n[dn]u+\n[.V]u
.ie \n[.t]<=\n[3dn] .tm error: page \n%: table will not fit on one page; use .TS H/.TH with a supporting macro package
.el \{.in 0
.ls 1
.nf
.if \n[ln] .nm \n[ln]
.3table
.\}
.rm 3table
.\}
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
..
.ec
.ce 0
.nf
.nr 3sep 1n
.nr 3w0 \n(.H
.nr 3aw0 0
.nr 3lnw0 0
.nr 3rnw0 0
.nr 3w1 \n(.H
.nr 3aw1 0
.nr 3lnw1 0
.nr 3rnw1 0
.nr 3w2 \n(.H
.nr 3aw2 0
.nr 3lnw2 0
.nr 3rnw2 0
.lf 47 1.man
.nr 3w0 \n[3w0]>?\w\[tbl]\-v|\-\-verbose\[tbl]
.lf 47
.nr 3w2 \n[3w2]>?\w\[tbl]logs everything\[tbl]
.lf 48
.nr 3w0 \n[3w0]>?\w\[tbl]\-q|\-\-quiet\[tbl]
.lf 48
.nr 3w2 \n[3w2]>?\w\[tbl]default level, outputs some logs\[tbl]
.lf 49
.nr 3w0 \n[3w0]>?\w\[tbl]\-s|\-\-silent\[tbl]
.lf 49
.nr 3w2 \n[3w2]>?\w\[tbl]log output completely surpressed\[tbl]
.nr 3w0 \n[3w0]>?(\n[3lnw0]+\n[3rnw0])
.if \n[3aw0] .nr 3w0 \n[3w0]>?(\n[3aw0]+2n)
.nr 3w1 \n[3w1]>?(\n[3lnw1]+\n[3rnw1])
.if \n[3aw1] .nr 3w1 \n[3w1]>?(\n[3aw1]+2n)
.nr 3w2 \n[3w2]>?(\n[3lnw2]+\n[3rnw2])
.if \n[3aw2] .nr 3w2 \n[3w2]>?(\n[3aw2]+2n)
.nr 3expand \n[.l]-\n[.i]-\n[3w0]-\n[3w1]-\n[3w2]-6n
.if \n[3expand]<0 \{\
.lf 47
.ig
.EQ
delim off
.EN
..
.tm1 "warning: file `\n[.F]', around line \n[.c]:
.tm1 "  table wider than line width
.ig
.EQ
delim on
.EN
..
.nr 3expand 0
.\}
.nr 3cd0 0
.nr 3cl0 0*\n[3sep]
.nr 3ce0 \n[3cl0]+\n[3w0]
.nr 3cl1 \n[3ce0]+(3*\n[3sep])
.nr 3cd1 \n[3ce0]+\n[3cl1]/2
.nr 3ce1 \n[3cl1]+\n[3w1]
.nr 3cl2 \n[3ce1]+(3*\n[3sep])
.nr 3cd2 \n[3ce1]+\n[3cl2]/2
.nr 3ce2 \n[3cl2]+\n[3w2]
.nr 3cd3 \n[3ce2]+(0*\n[3sep])
.nr TW \n[3cd3]
.if \n[3cent] \{.in +(u;\n[.l]-\n[.i]-\n[TW]/2>?-\n[.i])
.nr 3ind \n[.i]
.\}
.eo
.ig
.EQ
delim off
.EN
..
.de T#
.if !\n[3supbot] \{.3rvpt 0
.mk 3vert
.3rlnx s
.ls 1
.3rlnx r
.ls
.nr 3passed \n[3crow]
.sp |\n[3vert]u
.3rvpt 1
.\}
..
.ig
.EQ
delim on
.EN
..
.ec
.fc 
.3keep
.3rmk 3rt0
\*[3trans].nr 3crow 0
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs0
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 47
\&\h'|\n[3cl0]u'\-v|\-\-verbose\h'|\n[3cl2]u'logs everything
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs0]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt1
\*[3trans].nr 3crow 1
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs1
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 48
\&\h'|\n[3cl0]u'\-q|\-\-quiet\h'|\n[3cl2]u'default level, outputs some logs
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs1]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt2
\*[3trans].nr 3crow 2
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs2
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 49
\&\h'|\n[3cl0]u'\-s|\-\-silent\h'|\n[3cl2]u'log output completely surpressed
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs2]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.if \n[ln] .nr ln \n[3lnmx]
.3release
.mk 3rt3
.nr 3brule 1
.nr T. 1
.ig
.EQ
delim off
.EN
..
.T#
.ig
.EQ
delim on
.EN
..
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
.3init
.fc
.cp \n(3c
.lf 50
.TE
.SS other options
.TS
.nr 3c \n(.C
.cp 0
.nr 3lps \n[.s]
.nr 3cent \n[.ce]
.de 3init
.ft \n[.f]
.ps \n[.s]
.vs \n[.v]u
.in \n[.i]u
.ll \n[.l]u
.ls \n[.L]
.ad \n[.j]
.ie \n[.u] .fi
.el .nf
.ce \n[.ce]
..
.nr 3ind \n[.i]
.nr 3fnt \n[.f]
.nr 3sz \n[.s]
.nr 3fll \n[.u]
.nr T. 0
.nr 3crow 0-1
.nr 3passed 0-1
.nr 3sflag 0
.ds 3trans
.ds 3quote
.nr 3brule 1
.nr 3supbot 0
.eo
.de 3rmk
.mk \$1
.if !'\n(.z'' \!.3rmk "\$1"
..
.de 3rvpt
.vpt \$1
.if !'\n(.z'' \!.3rvpt "\$1"
..
.de 3rlns
.ie !'\n(.z'' \{.nm
\!.3rlns "\$1"
.\}
.el .if \n[ln] \{\
.if '\$1'd' .nr 3lnst \n[ln]
.if '\$1's' .nm \n[3lnst]
.if '\$1'm' .nr 3lnmx \n[ln]>?\n[3lnmx]
.\}
..
.de 3rlnx
.ie !'\n(.z'' \{.nm
\!.3rlnx "\$1"
.\}
.el .if \n[ln] \{\
.ie '\$1's' \{\
.nr 3lnsv \n(ln<?\n[3lnmx]
.nm +0 \n[ln]+42
.\}
.el \{\
.nr ln \n[3lnsv]
.nm \n[ln] 1
.\}
.\}
..
.de 3keep
.if '\n[.z]'' \{.ds 3quote \\
.ds 3trans \!
.di 3section
.nr 3sflag 1
.in 0
.\}
..
.ig
.EQ
delim off
.EN
..
.de 3release
.if \n[3sflag] \{.di
.in \n[3ind]u
.nr 3dn \n[dn]
.ds 3quote
.ds 3trans
.nr 3sflag 0
.if \n[.t]<=\n[dn] \{.nr T. 1
.T#
.nr 3supbot 1
.sp \n[.t]u
.nr 3supbot 0
.mk #T
.\}
.if \n[.t]<=\n[3dn] .tm warning: page \n%: table text block will not fit on one page
.nf
.if \n[ln] .nm \n[ln]
.nr 3lnmx \n[ln]
.ls 1
.3section
.ls
.if \n[ln] .nm
.rm 3section
.\}
..
.ig
.EQ
delim on
.EN
..
.nr 3tflag 0
.de 3tkeep
.if '\n[.z]'' \{.di 3table
.nr 3tflag 1
.\}
..
.de 3trelease
.if \n[3tflag] \{.br
.di
.nr 3dn \n[dn]
.ne \n[dn]u+\n[.V]u
.ie \n[.t]<=\n[3dn] .tm error: page \n%: table will not fit on one page; use .TS H/.TH with a supporting macro package
.el \{.in 0
.ls 1
.nf
.if \n[ln] .nm \n[ln]
.3table
.\}
.rm 3table
.\}
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
..
.ec
.ce 0
.nf
.nr 3sep 1n
.nr 3w0 \n(.H
.nr 3aw0 0
.nr 3lnw0 0
.nr 3rnw0 0
.nr 3w1 \n(.H
.nr 3aw1 0
.nr 3lnw1 0
.nr 3rnw1 0
.nr 3w2 \n(.H
.nr 3aw2 0
.nr 3lnw2 0
.nr 3rnw2 0
.lf 55 1.man
.nr 3w0 \n[3w0]>?\w\[tbl]\-h|\-\-help\[tbl]
.lf 55
.nr 3w2 \n[3w2]>?\w\[tbl]redirects to this manual page\[tbl]
.lf 56
.nr 3w0 \n[3w0]>?\w\[tbl]\-\-version\[tbl]
.lf 56
.nr 3w2 \n[3w2]>?\w\[tbl]outputs version string\[tbl]
.nr 3w0 \n[3w0]>?(\n[3lnw0]+\n[3rnw0])
.if \n[3aw0] .nr 3w0 \n[3w0]>?(\n[3aw0]+2n)
.nr 3w1 \n[3w1]>?(\n[3lnw1]+\n[3rnw1])
.if \n[3aw1] .nr 3w1 \n[3w1]>?(\n[3aw1]+2n)
.nr 3w2 \n[3w2]>?(\n[3lnw2]+\n[3rnw2])
.if \n[3aw2] .nr 3w2 \n[3w2]>?(\n[3aw2]+2n)
.nr 3expand \n[.l]-\n[.i]-\n[3w0]-\n[3w1]-\n[3w2]-6n
.if \n[3expand]<0 \{\
.lf 55
.ig
.EQ
delim off
.EN
..
.tm1 "warning: file `\n[.F]', around line \n[.c]:
.tm1 "  table wider than line width
.ig
.EQ
delim on
.EN
..
.nr 3expand 0
.\}
.nr 3cd0 0
.nr 3cl0 0*\n[3sep]
.nr 3ce0 \n[3cl0]+\n[3w0]
.nr 3cl1 \n[3ce0]+(3*\n[3sep])
.nr 3cd1 \n[3ce0]+\n[3cl1]/2
.nr 3ce1 \n[3cl1]+\n[3w1]
.nr 3cl2 \n[3ce1]+(3*\n[3sep])
.nr 3cd2 \n[3ce1]+\n[3cl2]/2
.nr 3ce2 \n[3cl2]+\n[3w2]
.nr 3cd3 \n[3ce2]+(0*\n[3sep])
.nr TW \n[3cd3]
.if \n[3cent] \{.in +(u;\n[.l]-\n[.i]-\n[TW]/2>?-\n[.i])
.nr 3ind \n[.i]
.\}
.eo
.ig
.EQ
delim off
.EN
..
.de T#
.if !\n[3supbot] \{.3rvpt 0
.mk 3vert
.3rlnx s
.ls 1
.3rlnx r
.ls
.nr 3passed \n[3crow]
.sp |\n[3vert]u
.3rvpt 1
.\}
..
.ig
.EQ
delim on
.EN
..
.ec
.fc 
.3keep
.3rmk 3rt0
\*[3trans].nr 3crow 0
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs0
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 55
\&\h'|\n[3cl0]u'\-h|\-\-help\h'|\n[3cl2]u'redirects to this manual page
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs0]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt1
\*[3trans].nr 3crow 1
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs1
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 56
\&\h'|\n[3cl0]u'\-\-version\h'|\n[3cl2]u'outputs version string
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs1]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.if \n[ln] .nr ln \n[3lnmx]
.3release
.mk 3rt2
.nr 3brule 1
.nr T. 1
.ig
.EQ
delim off
.EN
..
.T#
.ig
.EQ
delim on
.EN
..
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
.3init
.fc
.cp \n(3c
.lf 57
.TE
.RE
.SH EXIT STATUS
.RS 1
.TS
.nr 3c \n(.C
.cp 0
.nr 3lps \n[.s]
.nr 3cent \n[.ce]
.de 3init
.ft \n[.f]
.ps \n[.s]
.vs \n[.v]u
.in \n[.i]u
.ll \n[.l]u
.ls \n[.L]
.ad \n[.j]
.ie \n[.u] .fi
.el .nf
.ce \n[.ce]
..
.nr 3ind \n[.i]
.nr 3fnt \n[.f]
.nr 3sz \n[.s]
.nr 3fll \n[.u]
.nr T. 0
.nr 3crow 0-1
.nr 3passed 0-1
.nr 3sflag 0
.ds 3trans
.ds 3quote
.nr 3brule 1
.nr 3supbot 0
.eo
.de 3rmk
.mk \$1
.if !'\n(.z'' \!.3rmk "\$1"
..
.de 3rvpt
.vpt \$1
.if !'\n(.z'' \!.3rvpt "\$1"
..
.de 3rlns
.ie !'\n(.z'' \{.nm
\!.3rlns "\$1"
.\}
.el .if \n[ln] \{\
.if '\$1'd' .nr 3lnst \n[ln]
.if '\$1's' .nm \n[3lnst]
.if '\$1'm' .nr 3lnmx \n[ln]>?\n[3lnmx]
.\}
..
.de 3rlnx
.ie !'\n(.z'' \{.nm
\!.3rlnx "\$1"
.\}
.el .if \n[ln] \{\
.ie '\$1's' \{\
.nr 3lnsv \n(ln<?\n[3lnmx]
.nm +0 \n[ln]+42
.\}
.el \{\
.nr ln \n[3lnsv]
.nm \n[ln] 1
.\}
.\}
..
.de 3keep
.if '\n[.z]'' \{.ds 3quote \\
.ds 3trans \!
.di 3section
.nr 3sflag 1
.in 0
.\}
..
.ig
.EQ
delim off
.EN
..
.de 3release
.if \n[3sflag] \{.di
.in \n[3ind]u
.nr 3dn \n[dn]
.ds 3quote
.ds 3trans
.nr 3sflag 0
.if \n[.t]<=\n[dn] \{.nr T. 1
.T#
.nr 3supbot 1
.sp \n[.t]u
.nr 3supbot 0
.mk #T
.\}
.if \n[.t]<=\n[3dn] .tm warning: page \n%: table text block will not fit on one page
.nf
.if \n[ln] .nm \n[ln]
.nr 3lnmx \n[ln]
.ls 1
.3section
.ls
.if \n[ln] .nm
.rm 3section
.\}
..
.ig
.EQ
delim on
.EN
..
.nr 3tflag 0
.de 3tkeep
.if '\n[.z]'' \{.di 3table
.nr 3tflag 1
.\}
..
.de 3trelease
.if \n[3tflag] \{.br
.di
.nr 3dn \n[dn]
.ne \n[dn]u+\n[.V]u
.ie \n[.t]<=\n[3dn] .tm error: page \n%: table will not fit on one page; use .TS H/.TH with a supporting macro package
.el \{.in 0
.ls 1
.nf
.if \n[ln] .nm \n[ln]
.3table
.\}
.rm 3table
.\}
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
..
.ec
.ce 0
.nf
.nr 3sep 1n
.nr 3w0 \n(.H
.nr 3aw0 0
.nr 3lnw0 0
.nr 3rnw0 0
.nr 3w1 \n(.H
.nr 3aw1 0
.nr 3lnw1 0
.nr 3rnw1 0
.nr 3w2 \n(.H
.nr 3aw2 0
.nr 3lnw2 0
.nr 3rnw2 0
.lf 65 1.man
.nr 3w0 \n[3w0]>?\w\[tbl]\fBNAME\fR\[tbl]
.lf 65
.nr 3w1 \n[3w1]>?\w\[tbl]\fBVALUE\fR\[tbl]
.lf 65
.nr 3w2 \n[3w2]>?\w\[tbl]\fBDESCRIPTION\fR\[tbl]
.lf 66
.nr 3w0 \n[3w0]>?\w\[tbl]\fIEXIT_SUCCESS\fR\[tbl]
.lf 66
.nr 3tbw1,1 0\w\[tbl]0\[tbl]
.nr 3lnw1 \n[3lnw1]>?\n[3tbw1,1]
.lf 66
.nr 3w2 \n[3w2]>?\w\[tbl]data generation finished\[tbl]
.lf 67
.nr 3w0 \n[3w0]>?\w\[tbl]\fIERR_NO_CONF\fR\[tbl]
.lf 67
.nr 3tbw2,1 0\w\[tbl]2\[tbl]
.nr 3lnw1 \n[3lnw1]>?\n[3tbw2,1]
.lf 67
.nr 3w2 \n[3w2]>?\w\[tbl]insufficient or inconsistent configuration\[tbl]
.lf 68
.nr 3w0 \n[3w0]>?\w\[tbl]\fIERR_NO_ACCESS\fR\[tbl]
.lf 68
.nr 3tbw3,1 0\w\[tbl]3\[tbl]
.nr 3lnw1 \n[3lnw1]>?\n[3tbw3,1]
.lf 68
.nr 3w2 \n[3w2]>?\w\[tbl]invalid file access privilege\[tbl]
.lf 69
.nr 3w0 \n[3w0]>?\w\[tbl]\fIERR_CLASS_NULL\fR\[tbl]
.lf 69
.nr 3tbw4,1 0\w\[tbl]\-4\[tbl]
.nr 3lnw1 \n[3lnw1]>?\n[3tbw4,1]
.lf 69
.nr 3w2 \n[3w2]>?\w\[tbl]unknown entity name requested\[tbl]
.lf 70
.nr 3w0 \n[3w0]>?\w\[tbl]\fIERR_CLASS_PARSE\fR\[tbl]
.lf 70
.nr 3tbw5,1 0\w\[tbl]\-9\[tbl]
.nr 3lnw1 \n[3lnw1]>?\n[3tbw5,1]
.lf 70
.nr 3w2 \n[3w2]>?\w\[tbl]error parsing input file\[tbl]
.nr 3w0 \n[3w0]>?(\n[3lnw0]+\n[3rnw0])
.if \n[3aw0] .nr 3w0 \n[3w0]>?(\n[3aw0]+2n)
.nr 3w1 \n[3w1]>?(\n[3lnw1]+\n[3rnw1])
.if \n[3aw1] .nr 3w1 \n[3w1]>?(\n[3aw1]+2n)
.nr 3w2 \n[3w2]>?(\n[3lnw2]+\n[3rnw2])
.if \n[3aw2] .nr 3w2 \n[3w2]>?(\n[3aw2]+2n)
.nr 3expand \n[.l]-\n[.i]-\n[3w0]-\n[3w1]-\n[3w2]-6n
.if \n[3expand]<0 \{\
.lf 65
.ig
.EQ
delim off
.EN
..
.tm1 "warning: file `\n[.F]', around line \n[.c]:
.tm1 "  table wider than line width
.ig
.EQ
delim on
.EN
..
.nr 3expand 0
.\}
.nr 3cd0 0
.nr 3cl0 0*\n[3sep]
.nr 3ce0 \n[3cl0]+\n[3w0]
.nr 3cl1 \n[3ce0]+(3*\n[3sep])
.nr 3cd1 \n[3ce0]+\n[3cl1]/2
.nr 3ce1 \n[3cl1]+\n[3w1]
.nr 3cl2 \n[3ce1]+(3*\n[3sep])
.nr 3cd2 \n[3ce1]+\n[3cl2]/2
.nr 3ce2 \n[3cl2]+\n[3w2]
.nr 3cd3 \n[3ce2]+(0*\n[3sep])
.nr TW \n[3cd3]
.if \n[3cent] \{.in +(u;\n[.l]-\n[.i]-\n[TW]/2>?-\n[.i])
.nr 3ind \n[.i]
.\}
.eo
.ig
.EQ
delim off
.EN
..
.de T#
.if !\n[3supbot] \{.3rvpt 0
.mk 3vert
.3rlnx s
.ls 1
.3rlnx r
.ls
.nr 3passed \n[3crow]
.sp |\n[3vert]u
.3rvpt 1
.\}
..
.ig
.EQ
delim on
.EN
..
.ec
.fc 
.3keep
.3rmk 3rt0
\*[3trans].nr 3crow 0
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs0
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 65
\&\h'|\n[3cl0]u'\fBNAME\fR\h'|\n[3cl1]u'\fBVALUE\fR\h'|\n[3cl2]u'\fBDESCRIPTION\fR
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs0]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt1
\*[3trans].nr 3crow 1
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs1
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 66
\&\h'|\n[3cl0]u'\fIEXIT_SUCCESS\fR\h'|(\n[3w1]u-\n[3lnw1]u-\n[3rnw1]u/2u+\n[3lnw1]u+\n[3cl1]u-\n[3tbw1,1]u)'0\h'|\n[3cl2]u'data generation finished
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs1]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt2
\*[3trans].nr 3crow 2
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs2
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 67
\&\h'|\n[3cl0]u'\fIERR_NO_CONF\fR\h'|(\n[3w1]u-\n[3lnw1]u-\n[3rnw1]u/2u+\n[3lnw1]u+\n[3cl1]u-\n[3tbw2,1]u)'2\h'|\n[3cl2]u'insufficient or inconsistent configuration
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs2]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt3
\*[3trans].nr 3crow 3
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs3
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 68
\&\h'|\n[3cl0]u'\fIERR_NO_ACCESS\fR\h'|(\n[3w1]u-\n[3lnw1]u-\n[3rnw1]u/2u+\n[3lnw1]u+\n[3cl1]u-\n[3tbw3,1]u)'3\h'|\n[3cl2]u'invalid file access privilege
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs3]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt4
\*[3trans].nr 3crow 4
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs4
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 69
\&\h'|\n[3cl0]u'\fIERR_CLASS_NULL\fR\h'|(\n[3w1]u-\n[3lnw1]u-\n[3rnw1]u/2u+\n[3lnw1]u+\n[3cl1]u-\n[3tbw4,1]u)'\-4\h'|\n[3cl2]u'unknown entity name requested
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs4]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.if \n[ln] .nr ln \n[3lnmx]
.3keep
.3rmk 3rt5
\*[3trans].nr 3crow 5
.3keep
.3rlns d
.nr 3lnmx \n[ln]
.mk 3rs5
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce2]u
.lf 70
\&\h'|\n[3cl0]u'\fIERR_CLASS_PARSE\fR\h'|(\n[3w1]u-\n[3lnw1]u-\n[3rnw1]u/2u+\n[3lnw1]u+\n[3cl1]u-\n[3tbw5,1]u)'\-9\h'|\n[3cl2]u'error parsing input file
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs5]u
.3rlns m
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.if \n[ln] .nr ln \n[3lnmx]
.3release
.mk 3rt6
.nr 3brule 1
.nr T. 1
.ig
.EQ
delim off
.EN
..
.T#
.ig
.EQ
delim on
.EN
..
.if \n[ln] \{.nm
.nr ln \n[3lnmx]
.\}
.3init
.fc
.cp \n(3c
.lf 71
.TE
.RE
.SH FILES
.RS 1
.RE
.SH NOTES
.RS 1
.SS recursive parsing
configuration file for directory can be overriden at each recursion level
.RE
.SH EXAMPLE
.RS 1
.SS pyseeder \-\-version
.EE
displays version string
.SS pyseeder \-h
.EE
displays this manual
.SS
pyseeder \-\-config \.pyseeder.conf
.EE
creates data structure using knowledge base specified in \.pyseeder\.conf
.EE
generates data as specified in \.pyseeder\.conf
.EE
outputs as JSON files as specified in \.pyseeder\.conf
.EE
default console output
.SS
pyseeder \-s \-o test \-i res/Employees\.json res/Schedules\.json \-c Employee 5 \-c Schedule
.EE
creates data structure using res/Employees\.json and res/Schedules\.json
.EE
generates 5 instances of Employee and 1 Schedule \(assuming they are defined somewhere in resource files\)
.EE
outputs as JSON files under test/<class name>
.EE
suppresses console output
.SS
pyseeder \-v \-d res \-c generator.conf
.EE
creates data structure recursively parsing res/ \(as specified at each level\)
.EE
generates data as specified in generator\.conf
.EE
outputs as JSON files under \./<class name>
.EE
verboses console output
.RE

