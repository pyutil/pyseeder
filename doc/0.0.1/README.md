# PySeeder
## version 0.0.1
### About
**PySeeder** parses user-defined data entity templates to create internal class 
hierarchy, which it in turn uses to generate corresponding JSON data files folowing 
generator configuration supplied via command-line or a config file.

### Table of Contents

- [usage](#usage)
  - [synopsis](#synopsis)
  - [options](#options)
  - [exit codes](#exit-status)
  - [examples](#example)
- [support](#example)
  - [requirements](#prerequisites)
  - [installation](#installation)
- [description](#description)
- [structure](#project-structure)

### Usage
#### Synopsis
**pyseeder** -h|--version <br/>
**pyseeder** \[-vqs\] --config *file* <br/>
**pyseeder** \[-vqs\] \[-o *dir*\] \[-i *files...*|-d *dir*\] -c *file*|\(-c *class* *num*\) <br/>
#### Options
###### operation config

<table border="0">
   <tr>
      <td align="right">--config</td>
      <td align="center">file</td>
      <td>specifies configuration file to use for generation, overrides any other 
      config option</td>
   </tr>
   <tr>
      <td align="right">-o|--output</td>
      <td align="center">dir</td>
      <td>specifies directory under which output files are to be stored or stdout 
      if null</td>
   </tr> 
   <tr>     
      <td align="right">-i|--input</td>
      <td align="center">files...</td>
      <td>tells PySeeder to parse and register everything contained in files</td>
   </tr>
   <tr>
      <td align="right">-d|--directory</td>
      <td align="center">dir</td>
      <td>tells PySeeder to recursively parse everything contained in $dir</td>
   </tr>
   <tr>
      <td align="right" rowspan="2">-c|--create</td>
      <td align="center">class num</td>
      <td>signalises that $num instances of $class are to be created, $class must 
      be previously registered</td>
   </tr>
   <tr>
      <td align="center">file</td>
      <td>specifies generation command file</td>
   </tr>
</table>

###### console logging

<table border="0">
   <tr>
      <td align="right">-v|--verbose</td>
      <td>outputs lots of useful information</td>
   </tr>
   <tr>
      <td align="right">-q|--quiet</td>
      <td>default, outputs some information</td>
   </tr>
   <tr>
      <td align="right">-s|--silent</td>
      <td>completely suppresses console output</td>
   </tr>
</table>

###### other options

<table border="0">
   <tr>
      <td align="right">-h|--help</td>            
      <td>equivalent to `man 1 pyseeder`</td>
   </tr>
   <tr>
      <td align="right">-v|--version</td>
      <td>outputs version string</td>
   </tr>
</table>

#### Exit status
<table border="0">
   <thead>
      <tr>
         <td align="center">NAME</th>
         <td align="center">VALUE</th>
         <td align="center">DESCRIPTION</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td align="right">EXIT_SUCCESS</td>
         <td align="center">0</td>
         <td>data generation finished</td>
      </tr>
      <tr>
         <td align="right">EXIT_NO_CONF</td>
         <td align="center">2</td>
         <td>insuficient or inconsistent configuration</td>
      </tr>
      <tr>
         <td align="right">EXIT_NO_ACCESS</td>
         <td align="center">3</td>
         <td>invalid file access privileges</td>
      </tr>
      <tr>
         <td align="right">ERR_CLASS_NULL</td>
         <td align="center">-4</td>
         <td>attempt to create non-registered entity</td>
      </tr>
      <tr>
         <td align="right">ERR_CLASS_PARSE</td>
         <td align="center">-9</td>
         <td>error parsing input file</td>
      </tr>
   </tbody>
</table>

#### Example
- `pyseeder --version` <br/>displays version string <br/>
- `pyseeder -h` <br/>displays manpage(1) <br/>
- `pyseeder --config .pyseeder.conf` <br/>creates data structure using knowledge base 
specified in **.pyseeder.conf** <br/>
- `pyseeder -s -o test -i res/Employees.json res/Schedules.json -c Employee 5 -c Schedule` <br/>
creates data structure using **res/Employees.json** and **res/Schedules.json**, 
generates five instances of *Employee* (assuming it's defined somewhere in resource 
files) and one *Schedule* and outputs them as JSON files under **test/\<class name\>**, 
suppressing console output <br/>
- `pyseeder -v -d res -c generator.conf` <br/>creates data structure recursively parsing 
**res/** \(as specified at each level\), generates data as specified in **generator.conf** 
and outputs it under **./\<class name\>**, logging every action to console <br/>

### Support
#### Prerequisites
- Linux kernel >3.8.0
- Python interpreter >3.6.0

#### Installation
`cd /opt` <br/>
`git clone https://gitlab.com/pyutil/pyseeder` <br/>
`ln -s /opt/pyseeder <link name>` 

### Description
For each *Entity* comprised of a list of *fields* **PySeeder** generates objects 
applying reverse finite automata to set of instructions specifying each *field* 
generation formula, keeping track of every object it generates. *Entities* are 
defined in JSON files (currently on one-per-file basis) adhering to [**pattern**](./INPUT.md) 
accepted by [*InputParser*](../../src/InputParser) which are processed following
order of specification (conflicting *Entity* names result in override).

### Project structure