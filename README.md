# PySeeder
## version 0.0.1
### Goal:
This project is to provide flexible test-data generation framework - ultimate target customization level comprises of user-defined grammatics for regex deduction, user-defined data template format (as opposed to default JSON) with corresponding interpreter, and output persistence method (e.g. SQL database handler)
### Goal ([current](doc/0.0.1))
##### - [ ] min:
- [ ] write documentation
- [ ] implement reverse automata structure
- [ ] implement JSON input parsing
##### - [ ] target:
- [ ] define meta-language basics
- [ ] define .config standards
- [ ] perform tests
